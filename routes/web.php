<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Table crud with js
Route::get('/home','HomeController@table');
Route::post('/user','HomeController@user_insert');
Route::post('/edit/{id}','HomeController@user_edit');
Route::get('/delete/{id}','HomeController@user_delete');
Route::get('/deletes/{id}','HomeController@alldelete');


//AJAX data in mysql
Route::get('/ajax','HomeController@ajax');

//Drag 
Route::view('/drag','drag');