<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');  
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //---------------Function Index------------//

    public function ajax()
    {
        $data = User::all();
        return  $data;
    }
    public function user()
    {
    }

    public function table()
    {
        return view('table');
    }


    //-----------------Function Users insert ----------------//

    public function user_insert(Request $request)
    {

        //     // return "succes";
        //     // dd("salom");
        // //     $request->validate([
        // //         'name'=>'required | min:3 | max:30',
        // //         'email'=>'requred | min:5 | max :100 |unique:App\User,email_address | regex:/^.+@.+$/i',
        // //         'password'=>'required | password'
        // //     ]);
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;

        $data = new User();
        $data->name = $name;
        $data->email = $email;
        $data->password = Hash::make($password);
        $data->save();
        return $data;
    }

    //------------Function users delete----------//

    public function user_delete(Request $re, $id)
    {

        // return $id;
        $data = User::where('id', $id)->delete();
        return $data;
    }

    //------------------Function -> users edit------------------//

    public function user_edit(Request $request, $id)
    {
        //     $request->validate([
        //         'name'=>'required | min:3 | max:30',
        //         'email'=>'requred | min:5 | max :30 |unique:App\User,email_address | regex:/^.+@.+$/i',
        //         'password'=>'required | password'
        //     ]);
        $name = $request->name;
        $email = $request->email;
        User::where('id', $id)->update(['name' => $name, 'email' => $email]);
    }
//--------------------Function user all delete---------------------//
public function alldelete(Request $request,$id){
    // $users_id = explode(',',$id);
    $id = json_decode($id);

    for ($i=0; $i <count( $id); $i++) { 
        $user = $id[$i]->id ;
        $data = User::where('id',$user)->delete();
    }

    
  return $data;

}

  



}
