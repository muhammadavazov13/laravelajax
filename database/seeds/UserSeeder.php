<?php
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Str;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name'=>'Sarvar','email'=>'sarvar@gmail.com','password'=>'sarvar77'],
            ['name'=>'Alisher','email'=>'alisher@gmail.com','password'=>'alisher77'],
            ['name'=>'Botir','email'=>'botir@gmail.com','password'=>'botir77'],
            ['name'=>'Shahrizod','email'=>'shahrizod@gmail.com','password'=>'shahrizod77'],
            ['name'=>'Komil','email'=>'komil@gmail.com','password'=>'komil77'],
            ['name'=>'Dilshod','email'=>'dilshod@gmail.com','password'=>'dilshod77'],
            ['name'=>'Laziz','email'=>'laziz@gmail.com','password'=>'laziz'],
            ['name'=>'Alijon','email'=>'Alijon@gmail.com','password'=>'alijon'],
            ['name'=>'Asliddin','email'=>'asliddin@gmail.com','password'=>'asliddin'],
            ['name'=>'Muhammadamin','email'=>'Muhammadavazov13@gmail.com','password'=>'20061308']


        ];
        $index=0;
        foreach($users as $user){
            $data = new User();
            $data->name = $users[$index]['name'];
            $data->email = $users[$index]['email'];
            $data->password = Hash::make( $users[$index]['password']);
            $data->save();
            $index++;             
           }           
        

           factory(App\User::class, 10)->create();
        
    
     }

    }