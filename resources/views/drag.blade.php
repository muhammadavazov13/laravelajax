<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/74eb95443d.js" crossorigin="anonymous"></script>
    <title>Document</title>
</head>

<body>
    <div class="wrapper">
        <div class="item">
            <span class="text">Draggable element one</span>
            <i class="fas fa-bars"></i>
        </div>
        <div class="item">
            <span class="text">Draggable element two</span>
            <i class="fas fa-bars"></i>
        </div>
        <div class="item">
            <span class="text">Draggable element three</span>
            <i class="fas fa-bars"></i>
        </div>
        <div class="item">
            <span class="text">Draggable element four</span>
            <i class="fas fa-bars"></i>
        </div>
        <div class="item">
            <span class="text">Draggable element five</span>
            <i class="fas fa-bars"></i>
        </div>
    </div>
</body>

</html>