<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form method="POST" enctype="multipart/form-data" id="form">
                            <div class="modal-body">
                                <div class="mb-3">
                                    <label for="formname" class="form-label">Name</label>
                                    <input type="name" class="form-control" aria-describedby="emailHelp" id="formname" name="name">
                                    <div id="emailHelp" class="form-text">Your name</div>
                                </div>
                                @error('name')
                                <div class="alert alert-danger">10 ta belgidan ko'p matn kiriting</div>
                                @enderror
                                <div class="mb-3">
                                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email">
                                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="save" onclick="editUSer()"> Save </button>
                            </div>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>