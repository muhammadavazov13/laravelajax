var data;
var edit_id = -1;

//--------------------------AJAX data upload -------------------//

function viewData() {
    const xhttp = new XMLHttpRequest();
    xhttp.onload = function() {
        data = JSON.parse(this.response)
        chiz(data)
    }
    xhttp.open('GET', 'http://127.0.0.1:8000/ajax');
    xhttp.send();
}

//--------------------View data window loaded -------------------//

window.addEventListener("DOMContentLoaded", function() {
    viewData()
})

//-----------------------------------Search -------------------//

window.onload = function() {
    document.getElementById('search').addEventListener('keyup', function() {
        // delete_users=[]
        // document.getElementById('alld').style.visibility = "hidden"

        var dataFilter = data
        let input = document.getElementById("search");
        let filter = input.value.toUpperCase();
        dataFilter = dataFilter.filter(function(val) {
            if (val.name.toUpperCase().indexOf(filter) > -1 || val.email.toUpperCase().indexOf(filter) > -1)
                return val;
        });
        chiz(dataFilter)
    });
}
var tableIndex = 1;
var checkboxes;
var deletes = [];
var delete_users = [];

//------------------------------Show data in table--------------------//

function chiz(info) {
    // console.log(info);
    tableIndex = 1
    let tableView = info.map(tableData => `
        <tr class="table-td">
        <td><input id="remember" name="remember" type="checkbox" class="alldelete " value="` + tableData.id + `" /></td>
            <td>` + (tableIndex++) + `</td>
            <td>` + tableData.name + `</td>
            <td>` + tableData.email + `</td>
            <td>
                <button  class="btn btn-danger" onclick="deleteForm(` + tableData.id + `)" data-toggle="modal" data-target="#deleteModal">Delete</button>
                <button  class="btn btn-primary"data-toggle="modal" data-target="#editModal" onclick="editform(` + tableData.id + `)">Edit</button>
            </td>
        </tr>
    `)
    tableView = tableView.join('')
    document.getElementById('table').innerHTML = tableView



    select()
}

function select() {
    this.checkboxes = document.querySelectorAll('.alldelete');
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener('change', function() {
            if (checkbox.checked == true) {
                let user = data
                deletes.push(checkbox.value)
                let delete_filter = user.filter(a => a.id == checkbox.value)
                if (delete_users.filter(v => v.id == checkbox.value).length < 1) {
                    delete_users.push(delete_filter[0])
                }
                document.getElementById('alld').style.visibility = "visible"

            } else {
                deletes = deletes.filter(e => {
                    if (e != checkbox.value)
                        return e
                })
                delete_users = delete_users.filter(remove => {
                        if (remove.id != checkbox.value) {
                            return remove
                        }
                    })
                    // console.log(deletes);
                if (deletes.length == 0) {
                    document.getElementById('alld').style.visibility = "hidden"
                }
            }
            // console.log(delete_users);
        })
    });
}

//---------------------------------Show delete user in modal--------------------//

var keepUser

function deleteForm(id) {
    let delete_filter = data.filter(a => a.id == id)
    keepUser = delete_users
    delete_users = []
    delete_users.push(delete_filter[0])
    bchiqar()
}

//------------------------------------Delete user --------------------------//

document.getElementById('deletebutton').addEventListener('click', function() {

    let users_id = keepUser
    let id = JSON.stringify(users_id)
    var http = new XMLHttpRequest()

    http.open('GET', 'http://127.0.0.1:8000/deletes/' + id, true);
    http.send(id);
    document.getElementById('alld').style.visibility = "hidden"
    keepUser = ''
    console.log(keepUser);
    this.checkboxes = document.querySelectorAll('.alldelete');
    checkboxes.forEach(function(checkbox) {
        checkbox.checked = false
    })
    viewData()

})

//--------------------------User Edit and Save ---------------------------//

document.getElementById('btnsave').addEventListener("click", function() {
    var xhr = new XMLHttpRequest()
    var form = document.getElementById('form')
    if (edit_id > 0) {
        let fd = new FormData(form);
        xhr.open('POST', 'http://127.0.0.1:8000/edit/' + edit_id, true);
        xhr.send(fd);
    } else {
        let fDAta = new FormData(form);
        xhr.open('POST', 'http://127.0.0.1:8000/user', true);
        xhr.send(fDAta);
    }
    viewData()

})

//--------------------------------------Modal vars ------------------------------//

var inputName = document.getElementById('nameInput');
var inputEmail = document.getElementById('emailInput');
var divPassword = document.getElementById('divpassword');
var editModal = document.getElementsByClassName('modal')[0]
var inputPassword = document.getElementById('passwordInput')
var modalBtn = document.getElementById('btnsave')
var modalTitle = document.getElementById('editModalLabel')

//---------------------------------Edit modal data--------------------------------//

function editform(value) {
    var editData = this.data.filter(info => info.id == value)[0]
    this.edit_id = editData.id
    inputName.value = editData.name
    inputEmail.value = editData.email
    divPassword.style.visibility = 'hidden'
    editModal.removeAttribute('id')
    editModal.setAttribute("id", "editModal")
    modalTitle.innerText = `Foydalanuvchi o'zgartirish `

}

//--------------------------------------Add modal data--------------------------------//

function addModal() {
    this.edit_id = -1;
    editModal.removeAttribute('id')
    editModal.setAttribute("id", "addModal")
    divPassword.style.visibility = 'visible'
    inputName.value = '';
    inputEmail.value = '';
    inputPassword.value = '';
    modalBtn.removeAttribute('id')
    modalBtn.setAttribute("id", "btnsave")
}

//----------------------------------------Select users delete -----------------------------//
// document.getElementById('close').addEventListener('click', function() {
//     if (keepUser) {
//         delete_users = keepUser
//     }
// })
// document.getElementById('closeicon').addEventListener('click', function() {
//     if (keepUser) {
//         delete_users = keepUser
//     }
// })

// })
document.getElementById("alld").addEventListener('click', function() {
    bchiqar()


})

function bchiqar() {
    let index = 1
    let index2 = 1
    var deleteData = delete_users.map(a => `
            <h6 id="deleteName">` + (index++) + ` .  Name : ` + a.name + `</h6><br>
            <h6 id="deleteEmail">` + (index2++) + ` . Email : ` + a.email + `</h6>
            `)
    deleteData.join('')
    document.getElementById('deleteText').innerHTML = deleteData
    if (deleteData.length - 2 < 0) {
        keepUser = delete_users
        delete_users = []
        this.checkboxes = document.querySelectorAll('.alldelete');
        checkboxes.forEach(function(checkbox) {
            checkbox.checked = false
        })
        document.getElementById('alld').style.visibility = "hidden"
    } else {
        keepUser = delete_users
        delete_users = []
    }
}